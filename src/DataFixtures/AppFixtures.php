<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{


    public function load(ObjectManager $manager): void
    {
        $description = 'Lorem ipsum dolor sit amet consectetur adipiscing elit urna, at fames vulputate nibh commodo habitant auctor bibendum, varius eros mauris sagittis venenatis tellus sollicitudin. Arcu pharetra gravida rhoncus fames netus maecenas potenti aliquam, integer inceptos nec purus placerat donec quam facilisis libero, senectus sagittis litora sed consequat dignissim vestibulum. Magna aenean molestie habitasse nulla maecenas, semper rutrum primis ligula donec, quam egestas cras aptent.';
        $user = new User();
        $user->setName("javi");
        $user->setDescription($description);
        $user->setUrl('https://media-exp1.licdn.com/dms/image/C5603AQFegiuT6RJ2sQ/profile-displayphoto-shrink_800_800/0/1560025154030?e=1644451200&v=beta&t=L7v0BMnKQTKrhy0vFAbmBcBjxHcm8XlEewtSQ620Rus');
        $manager->persist($user);
        $user2 = new User();
        $user2->setName("Pedro");
        $user2->setUrl('https://upload.wikimedia.org/wikipedia/commons/3/32/Chelsea_1_Southampton_3_%2821753895798%29.jpg');
        $user2->setDescription($description);
        $manager->persist($user2);
        $user3 = new User();
        $user3->setName("David");
        $user3->setDescription($description);
        $user3->setUrl('https://imagenes.20minutos.es/files/image_656_370/uploads/imagenes/2019/12/27/david-de-miguel-angel.png');
        $manager->persist($user3);
        $manager->flush();
    }
}
